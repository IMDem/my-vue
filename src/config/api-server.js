/**
 * Created by deyu on 2017/1/22.
 */
var merge = require('webpack-merge');
var express = require('express');
var api = express();
var url = {
  login: '/user/login', // 登录
  register: '/user/register', // 注册
  getAuthenticatedUser: '/user/me', //获取token对应的user信息，也用来做token验证刷新
  getMyRmbList: '/user/my-rmb/list', // 获取账单列表
  addMyRmb: '/user/add/my-rmb', // 添加账单
  getTagList: '/user/get/tag/list' // 获取账单已有标签列表
};
api.post(url.login, function (req, res){
  res.send({
    code: 0,
    msg: 'success',
    data: {
      access_token: 'access_token',
      user: {
        name: '测试登录',
        account: '123456',
        email: '123@qq.com',
        id: 2
      }
    }
  });
});
api.post(url.register, function (req, res){
  res.send({
    code: 0,
    msg: 'success',
    data: {
      access_token: 'access_token',
      user: {
        name: '测试登录',
        account: '123456',
        email: '123@qq.com',
        id: 2
      }
    }
  });
});

api.post(url.getAuthenticatedUser, function (req, res){
  res.set('Authorization','Bearer access_token');
  res.send({
    code: 0,
    msg: '',
    data: {
      user: {
        name: '测试登录',
        account: '123456',
        email: '123@qq.com',
        id: 2
      }
    }
  });
});
api.get(url.getMyRmbList, function (req, res) {
  res.set('Authorization','Bearer access_token');
  res.send({
    'code': 0,
    msg: 'success',
    data: {
      '2012-2': [
        {
          'tag': '吃',
          'desc': '吃你妈比',
          'title': '吃（-100.00）',
          'time': '2012-02-02 12:12:12'
        },
        {
          'tag': '吃',
          'desc': '吃你妈比',
          'title': '吃（-100.00）',
          'time': '2012-02-02 12:12:12'
        },
        {
          'tag': '吃',
          'desc': '吃你妈比',
          'title': '吃（-100.00）',
          'time': '2012-02-02 12:12:12'
        },
        {
          'tag': '吃',
          'desc': '吃你妈比',
          'title': '吃（-100.00）',
          'time': '2012-02-02 12:12:12'
        },
      ],
      '2012-3': [
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          },
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          },
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          },
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          },
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          }
        ]
    }
  });
});
api.post(url.addMyRmb,function (req, res) {
  res.send({
    code: 0,
    msg: 'success',
    data: [
      {
        time: '2012-2',
        list: [
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          },
        ]
      },
      {
        time: '2012-3',
        list: [
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          },
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          },
          {
            'tag': '吃',
            'desc': '吃你妈比',
            'title': '吃（-100.00）',
            'time': '2012-02-02 12:12:12'
          }
        ]
      }
    ]
  });
});
api.get(url.getTagList, function (req, res) {
  res.send({
    code: 0,
    msg: 'success',
    data: [
      {
        key: 1,
        value: '吃'
      },
      {
        key: 2,
        value: '玩'
      },
      {
        key: 3,
        value: '乐'
      },
      {
        key: 4,
        value: '喝'
      }
    ]
  });
});
module.exports = merge({
  api: api
});
