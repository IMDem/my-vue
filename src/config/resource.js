/**
 * Created by  on 2016/4/1.
 */
import Vue from 'vue';
import Axios from 'axios';
import VueAxios from 'vue-axios';
import apiUrl from './api-url';
import { getToken } from '../utils/auth-service';
import { TOKEN_HEADER_KEY_NAME, TOKEN_HEADER_VALUE_NAME } from './my-vue';

Vue.use(VueAxios, Axios);

// HTTP相关
// Vue.http.options.crossOrigin = true;
// Vue.axios.options.emulateJSON = true;
// Vue.http.options.xhr = { withCredentials: true };
// Vue.http.options.emulateHTTP = true;
// Vue.http.headers.common[TOKEN_HEADER_KEY_NAME] = getToken();
// Add a request interceptor
Vue.axios.interceptors.request.use((config) => {
  let token = getToken();
  if (token) {
    console.log('加载页面上的token：' + token);
    config.headers.common[TOKEN_HEADER_KEY_NAME] = TOKEN_HEADER_VALUE_NAME + ' ' + token;
  }
  // Do something before request is sent
  return config;
}, (error) => {
  // Do something with response error
  return Promise.reject(error);
});

// Add a response interceptor
Vue.axios.interceptors.response.use((response) => {
  // Do something with response data
  if (response.headers.authorization) {
    let tokenArr = response.headers.authorization.split(' ');
    if (tokenArr[1]) {
      response.data.data.access_token = tokenArr[1];
    }
  }
  return response;
}, (error) => {
  // Do something with response error
  return Promise.reject(error);
});
// 登录
export const login = (options) => Vue.axios.post(apiUrl.login, options);
// 注册
export const register = (options) => Vue.axios.post(apiUrl.register, options);
// 获取token对应user信息,也用做token认证
export const getAuthenticatedUser = (options) => Vue.axios.post(apiUrl.getAuthenticatedUser, options);

// 获取我的账单列表
export const getMyRmbList = (options) => Vue.axios.get(apiUrl.getMyRmbList, {
  params: options
});
// 添加账单
export const addMyRmb = (options) => Vue.axios.post(apiUrl.addMyRmb, options);
// 获取账单已有标签列表
export const getTagList = (options) => Vue.axios.get(apiUrl.getTagList, {
  params: options
});
export default {
  login,
  register,
  getAuthenticatedUser,
  getMyRmbList,
  addMyRmb,
  getTagList
};
