/**
 * Created by deyu on 2017/2/11.
 */
export const LOCAL_ACCESS_TOKEN_NAME = 'access_token';
export const LOCAL_USER_DATA_NAME = 'user_data';
export const TOKEN_HEADER_KEY_NAME = 'Authorization';
export const TOKEN_HEADER_VALUE_NAME = 'Bearer';
export default {
  LOCAL_ACCESS_TOKEN_NAME,
  LOCAL_USER_DATA_NAME,
  TOKEN_HEADER_KEY_NAME,
  TOKEN_HEADER_VALUE_NAME
};
