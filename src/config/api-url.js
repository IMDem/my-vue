/**
 * Created by deyu on 2017/1/23.
 */
const root = (process.env.NODE_ENV !== 'production') ? 'http://rclan.caideyu.dev/api' : 'http://api.caideyu.cn/api';
export default{
  login: root + '/user/login', // 登录
  register: root + '/user/register', // 注册
  getAuthenticatedUser: root + '/user/me', // 获取token对应的user信息，也用来做token验证刷新
  getMyRmbList: root + '/user/my-rmb/list', // 获取账单列表
  addMyRmb: root + '/user/add/my-rmb', // 添加账单
  getTagList: root + '/user/get/tag/list' // 获取账单已有标签列表
};
