// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import VueRouter from 'vue-router';
import Vux from 'vuex';
// import Vuex from 'vuex';
import store from './vuex/store';
import User from './components/User/User';
import Index from './components/Index/Index';
import MyRmb from './components/User/MyRmb/MyRmb';
import Login from './components/User/Login/Login';
import Register from './components/User/Register/Register';
import { isLogin } from './utils/auth-service';
Vue.use(VueRouter);
import { AlertPlugin, LoadingPlugin } from 'vux';
Vue.use(Vux);
Vue.use(AlertPlugin);
Vue.use(LoadingPlugin);

// Vue.use(Vuex);
const routes = [
  {
    path: '/',
    name: 'index',
    redirect: 'index',
    meta: {
      title: '正在加载。。。'
    }
  },
  {
    path: '/user',
    name: 'user',
    component: User,
    meta: {
      title: '用户页',
      requiresAuth: false
    },
    children: [
      {
        path: 'login',
        name: 'login',
        component: Login,
        meta: {
          title: '登录页'
        }
      },
      {
        path: 'register',
        name: 'register',
        component: Register,
        meta: {
          title: '注册页'
        }
      },
      {
        path: 'my-rmb',
        name: 'my-rmb',
        component: MyRmb,
        meta: {
          title: '账单页',
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/index',
    name: 'index',
    component: Index,
    meta: {
      title: '首页'
    }
  }
];
const router = new VueRouter({
  mode: 'history',
  // linkActiveClass: 'active',
  routes
});
router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  if (to.meta.requiresAuth) {
    if (!isLogin()) {
      next({
        name: 'login',
        query: { redirectName: to.name }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});
/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
