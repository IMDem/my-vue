/**
 * Created by deyu on 2017/2/9.
 */
import { LOCAL_ACCESS_TOKEN_NAME, LOCAL_USER_DATA_NAME } from '../config/my-vue';
export function saveLocalStorage (name, value) {
  window.localStorage.setItem(name, value);
}

export function getLocalStorage (name) {
  return window.localStorage.getItem(name);
}

export function removeLocalStorage (name) {
  return window.localStorage.removeItem(name);
}
// 更新token
export function updateToken (token) {
  window.localStorage.setItem(LOCAL_ACCESS_TOKEN_NAME, token);
}

// 更新user_data
export function updateUserData (data) {
  window.localStorage.setItem(LOCAL_USER_DATA_NAME, JSON.stringify(data));
}
// 退出登录
export function signOut () {
  window.localStorage.removeItem(LOCAL_ACCESS_TOKEN_NAME);
  window.localStorage.removeItem(LOCAL_USER_DATA_NAME);
}
// 是否登录
export function isLogin () {
  return !!window.localStorage.getItem(LOCAL_ACCESS_TOKEN_NAME);
}
// 获取token
export function getToken () {
  return window.localStorage.getItem(LOCAL_ACCESS_TOKEN_NAME);
}
// 获取用户信息
export function getUserData () {
  return JSON.parse(window.localStorage.getItem(LOCAL_USER_DATA_NAME));
}
