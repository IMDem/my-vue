/**
 * Created by deyu on 2017/1/23.
 */
import { GET_MYRMB_LIST, GET_TAG_LIST, ADD_MYRMB } from '../mutation-types';
const state = {
  myRmbList: [],
  tagData: {
    list: [],
    val: 1, // 标签的id
    type: false, // 是否为其他标签
    value: '' // 其他标签的字符串
  }
};
const mutations = {
  [GET_MYRMB_LIST] (state, data) {
    state.myRmbList = data;
  },
  [GET_TAG_LIST] (state, data) {
    state.tagData.list = data;
    state.val = data[0] ? data[0].key : 0;
  },
  [ADD_MYRMB] (state, data) {
    // todo::这里需要更改  刷新逻辑未加入
    state.myRmbList = data;
  }
};
export default {
  state,
  mutations
};
