/**
 * Created by deyu on 2017/1/24.
 */
import api from '../config/resource';
import * as types from './mutation-types';
// 登录
export const login = ({ commit, state }, data) => {
  return new Promise(function (resolve, reject) {
    api.login(data).then(response => {
      if (response.data.code === 0) {
        // commit(types.UPDATE_USER_STATUS, response.data.data);
        commit(types.UPDATE_USER_STATE, response.data.data);
        resolve({
          'isLogin': true
        });
      } else {
        resolve({
          'isLogin': false,
          'msg': response.data.msg
        });
      }
    }).catch(error => {
      console.log(error);
      resolve({
        'isLogin': false,
        'msg': '网络错误'
      });
    });
  });
};
// 注册
export const register = ({ commit, state }, data) => {
  return new Promise(function (resolve, reject) {
    api.register(data).then(response => {
      if (response.data.code === 0) {
        // commit(types.SET_LOCAL_ACCESS_TOKEN, response.body.data);
        // commit(types.SET_LOCAL_USER_DARA, response.body.data);
        commit(types.UPDATE_USER_STATE, response.data.data);
        resolve({
          'isSuccess': true
        });
      } else {
        resolve({
          'isSuccess': false,
          'msg': response.data.msg
        });
      }
    }).catch(error => {
      console.log(error);
      resolve({
        'isSuccess': false,
        'msg': '网络错误'
      });
    });
  });
};
export const signOut = ({ commit, state }) => {
  commit(types.SIGN_OUT);
};
// 获取token对应user信息,也用做token刷新
export const getAuthenticatedUser = ({ commit, state }) => {
  return new Promise(function (resolve, reject) {
    api.getAuthenticatedUser().then(response => {
      if (response.data.code === 0) {
        commit(types.UPDATE_USER_STATE, response.data.data);
        resolve({
          'isAuth': true
        });
      } else {
        resolve({
          'isAuth': false,
          'msg': response.data.msg
        });
      }
    }).catch(error => {
      console.log(error);
      resolve({
        'isAuth': false,
        'msg': '网络错误'
      });
    });
  });
};
// 获取我的账单列表
export const getMyRmbList = ({ commit, state }) => {
  return new Promise(function (resolve, reject) {
    api.getMyRmbList().then(response => {
      if (response.data.code === 0) {
        commit(types.GET_MYRMB_LIST, response.data.data);
        resolve({
          code: true
        });
      } else {
        resolve({
          code: false,
          msg: response.data.msg
        });
      }
    }).catch(error => {
      console.log(error);
      resolve({
        code: false,
        msg: '网络错误'
      });
    });
  });
};

// 获取账单已知标签
export const getTagList = ({ commit, state }) => {
  return new Promise(function (resolve, reject) {
    api.getTagList().then(response => {
      if (response.data.code === 0) {
        commit(types.GET_TAG_LIST, response.data.data);
        resolve({
          code: true
        });
      } else {
        resolve({
          code: false,
          msg: response.data.msg
        });
      }
    }).catch(error => {
      console.log(error);
      resolve({
        code: false,
        msg: '网络错误'
      });
    });
  });
};
// 添加账单
export const addMyRmb = ({ commit, state }, data) => {
  return new Promise(function (resolve, reject) {
    api.addMyRmb(data).then(response => {
      if (response.data.code === 0) {
        commit(types.ADD_MYRMB, response.data.data);
        resolve({
          code: true
        });
      } else {
        resolve({
          code: false,
          msg: response.data.msg
        });
      }
    }).catch(error => {
      console.log(error);
      resolve({
        code: false,
        msg: '网络错误'
      });
    });
  });
};

// const actions = {
//   [types.GET_MYRMB_LIST] (context, data) {
//     api
//       .getMyRmb()
//       .then(res => {
//         console.log(res);
//       })
//       .fail(err => {
//         console.log(err);
//       });
//   }
// };
