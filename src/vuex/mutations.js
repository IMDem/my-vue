/**
 * Created by deyu on 2017/2/17.
 */
import { UPDATE_USER_STATE, SIGN_OUT } from './mutation-types';
import { isLogin, getUserData, updateToken, updateUserData, signOut } from '../utils/auth-service';
export default {
  [UPDATE_USER_STATE] (state, data) {
    updateToken(data.access_token);
    updateUserData(data.user);
    state.isLogin = isLogin();
    state.user = getUserData();
  },
  [SIGN_OUT] (state) {
    signOut();
    state.isLogin = isLogin();
    state.user = getUserData();
  }
};
