/**
 * Created by deyu on 2017/1/23.
 */
import Vue from 'vue';
import Vuex from 'vuex';
import middlewares from './middlewares';
import myRmb from './modules/my-rmb';
import login from './modules/login';
import * as actions from './actions';
import state from './state';
import mutations from './mutations';
import * as getters from './getters';

const debug = process.env.NODE_ENV !== 'production';
Vue.use(Vuex);
Vue.config.debug = debug;
export default new Vuex.Store({
  state,
  actions,
  getters,
  mutations,
  modules: {
    myRmb,
    login
  },
  strict: debug,
  middlewares
});

