/**
 * Created by Administrator on 2017/3/11 0011.
 */
import { isLogin, getUserData } from '../utils/auth-service';
export default {
  isLogin: isLogin(),
  user: getUserData()
};
