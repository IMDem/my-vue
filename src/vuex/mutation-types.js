
// 查询组件types
// 退出登录
export const SIGN_OUT = 'SIGN_OUT';
// 更新用户数据和状态
export const UPDATE_USER_STATE = 'UPDATE_USER_STATE';
// 我的账单
export const GET_MYRMB_LIST = 'GET_MYRMB_LIST';
// 获取账单已有标签
export const GET_TAG_LIST = 'GET_TAG_LIST';
// 添加账单
export const ADD_MYRMB = 'ADD_MYRMB';

// export const SET_LOCAL_ACCESS_TOKEN = 'SET_LOCAL_ACCESS_TOKEN';
